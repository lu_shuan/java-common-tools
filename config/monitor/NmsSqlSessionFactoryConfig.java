package com.ztesoft.zsmart.zcm.monitor.config;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.sql.DataSource;

import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;

import com.ztesoft.zsmart.core.log.ZSmartLogger;

/**
 * TODO
 *
 * @Author FJJ
 * @Date 2018/7/4
 */
@Configuration
@AutoConfigureAfter(MybatisConfiguration.class)

@MapperScan(basePackages = "com.ztesoft.zsmart.zcm.monitor.mapper.nms", sqlSessionFactoryRef = "nmsSqlSessionFactory")
public class NmsSqlSessionFactoryConfig {
    
    /** The logger. */
    private ZSmartLogger logger = ZSmartLogger.getLogger(NmsSqlSessionFactoryConfig.class);

    /**
     * Sql session factory.
     *
     * @param dataSource the data source
     * @return the sql session factory bean
     */
    @Bean(name = "nmsSqlSessionFactory")
    SqlSessionFactoryBean sqlSessionFactory(@Qualifier("nmsDataSource") DataSource dataSource) {
        SqlSessionFactoryBean ssfb = new SqlSessionFactoryBean();
        ssfb.setDataSource(dataSource);
        ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();

        // //分页插件
        // PaginationInterceptor pageHelper = new PaginationInterceptor();
        // //添加插件
        // ssfb.setPlugins(new Interceptor[] {
        // (Interceptor) pageHelper
        // });

        try {
            List<Resource> resources = new ArrayList<>();
            resources.addAll(Arrays.asList(
                    resolver.getResources("classpath:com/ztesoft/zsmart/zcm/monitor/**/nms/**.xml")));
            resources.addAll(Arrays.asList(
                    resolver.getResources("classpath:com/ztesoft/zsmart/zcm/monitor/**/flowdb/**.xml")));
            Resource[] array = resources.toArray(new Resource[resources.size()]);
            ssfb.setMapperLocations(array);
        }
        catch (IOException e) {
            logger.error(e.getMessage(), e);
        }
        return ssfb;
    }
    //
    // @Bean(name = "nmsTransactionManager")
    // PlatformTransactionManager transactionManager(@Qualifier("nmsDataSource")DataSource dataSource) {
    // DataSourceTransactionManager transactionManager = new DataSourceTransactionManager();
    // transactionManager.setDataSource(dataSource);
    // return transactionManager;
    // }

}
