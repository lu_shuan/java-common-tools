package com.ztesoft.zsmart.zcm.monitor.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;
import java.util.Map;

/**
 * Created by lkj on 2018/4/09.
 */
@Configuration
@ConfigurationProperties(prefix = "zcm.monitor")
@Data
public class MonitorProperties {

    /** zmq 订阅是否开启. */
    boolean zmqSubscribeEnabled = true;

    /** zmq 服务端. */
    String[] zmqNamesrvAddrs;

    /** zmq 消费者. */
    String[] zmqConsumerIds;

    /**
     * 导出文件路径
     */
    String influxdbFileDirectory;

    /**
     * 单次导出数量
     */
    int influxdbBatchSize;

    /**
     * 主机监控file_system表忽略字段
     */
    String influxdbFilterField;

    String influxdbMasterIp;

    String influxdbSlaveIp;

    /**
     * 邮件发送间隔时间，默认15s
     */
    String sendIntervalTime;

    /**
     * agent同步队列处理线程池大小
     */
    Integer messageBrokerThreadPoolSize;

    Integer messageBrokerThreadMaxPoolSize;

    /**
     * 服务端告警任务线程池配置
     */
    Integer agentAlarmTaskThreadPoolSize;

    Integer agentAlarmTaskThreadMaxPoolSize;

    /**
     * agent是否开启自动更新
     */
    boolean agentAutoSync = true;

    String activemqPassword = "{cipher}{rsa}fuN1QznW+jS32litlI6pO2rISR32PwS0urV1Ds9tIZY=";

    Integer containerMonitorSplitNum;

    Integer hostMonitorSplitNum;

    Integer systemGeneralThreadPoolSize;

    Integer systemGeneralThreadMaxPoolSize;

    Integer jedisKeyExpireTime;

    Integer jedisKeyExpireTimeBlock;

    Integer alarmProcessLimit;

    String httpGrafanaUrl;

    String activemqWebPort;

    Boolean prometheusMetricsEnabled = false;

    String containerMonitorTime;

    String selfMonitorIpRule;

    String asoUrl;

    int asoTimeInterval;

    String asoAccesssecret;

    long influxdbConnectTimeOut;

    long influxdbReadTimeOut;

    Boolean openAllEnabled;

    Boolean gatewayNginxClickhouseQuerySwitch;

    String influxdbUrl;

    String influxdbUrlRead;

    String influxdbUser;

    String influxdbPassword;

    String redisHost;

    String redisPort;

    boolean containerScanEnabled = true;

    String postUrl;

    String serviceUrl;

    String dbserviceUrl;

    String grafanaUrl;

    String grafanaApiKey;

    public String activemqIp;

    public String actualActivemqIp;

    public String activemqPort;

    public String activemqJmxPort;

    public int sftpPort;

    public String sftpHomePath;

    public String sftpPassword;

    public String daemonSyncFilesList;

    public String sftpHost;

    String probeConfigFilePath;

    String ctrlTimeout;

    String messageCenterUrl;

    boolean zmqSubscribeDebugEnabled = false;

    String zmqNamesrvAddrDebug;

    String zmqConsumerIdDebug;

    boolean selfMonitorFlag;

    List<Map<String, String>> mqConfig;

    String dockerApiPort;

    String clouderaHost;

    String clouderaPort;

    String clouderaVersion;

    String clouderaUser;

    String clouderaPassword;

    String dataBaseUrl;

    String dataBaseUser;

    String dataBasePassword;

    String dataBaseFormName;

    String middlewareFilterResClsId;

    String collectdPostUrl;

    public String alarmTimingNum;

    public String filterProjectIds;

    public String cmdbBaseUrl;

    Integer kapacitorStatusRefreshTime;

    Integer kapacitorTaskRefreshTime;

    String hostMonitorViewSwitch;

    Integer httpAlarmLimitCount;

    Integer selfMonitorIntervalTime;

    String keepAlarmEventMonths;

    List<String> monitorScreenMiddlewareResClsIds;

    String cleanAlarmEventCron;

    String selfMonitorUrl;

    int serverPort;

    int influxdbReadPoolSize;

    int influxdbReadTimeoutSeconds;

    String zcampBaseUrl;

    int dingdingMarkdownActions;

    int dingdingMarkdownInterval;

    String dingdingMarkdownWebhook;

    String dialingBaseUrl;

    String dingdingMarkdownArea;

    Boolean enableAnalysisApp;

    String applicationBaseUrl;

    Boolean enableProjectUnknown;

    String kapacitorRefreshCodeRule;

    String monitorBaseUrl;

    String monitorDingUrl;

    Boolean amqEnabled;

    Boolean alarmEvnetInfluxdbEnabled;

    Boolean chEnabled;

    Boolean httpAlarmBlockEnabled;

    String httpAlarmBlockRegularRule;

    Boolean mailSimple;

    int mailSimpleSize;

    public List<String> getMonitorScreenMiddlewareResClsIds() {
        return monitorScreenMiddlewareResClsIds;
    }

    public void setMonitorScreenMiddlewareResClsIds(List<String> monitorScreenMiddlewareResClsIds) {
        this.monitorScreenMiddlewareResClsIds = monitorScreenMiddlewareResClsIds;
    }

    public boolean isAgentAutoSync() {
        return agentAutoSync;
    }

    public List<Map<String, String>> getMqConfig() {
        return mqConfig;
    }

    public void setMqConfig(List<Map<String, String>> mqConfig) {
        this.mqConfig = mqConfig;
    }

    public Boolean isPrometheusMetricsEnabled() {
        return prometheusMetricsEnabled;
    }

    public String[] getZmqNamesrvAddrs() {
        return zmqNamesrvAddrs.clone();
    }

    public boolean isZmqSubscribeEnabled() {
        return zmqSubscribeEnabled;
    }

    public void setZmqNamesrvAddrs(String zmqNamesrvAddrs) {
        this.zmqNamesrvAddrs = zmqNamesrvAddrs.split(";");
    }

    public String[] getZmqConsumerIds() {
        return zmqConsumerIds.clone();
    }

    public void setZmqConsumerId(String consumerIds) {
        this.zmqConsumerIds = consumerIds.split(";");
    }

    public boolean isContainerScanEnabled() {
        return containerScanEnabled;
    }

    public boolean isZmqSubscribeDebugEnabled() {
        return zmqSubscribeDebugEnabled;
    }

    public boolean isSelfMonitorFlag() {
        return selfMonitorFlag;
    }
}
