package com.ztesoft.zsmart.zcm.monitor.config;

import com.ztesoft.zsmart.core.log.ZSmartLogger;
import com.ztesoft.zsmart.zcm.monitor.model.sharding.AlarmEvent;
import com.ztesoft.zsmart.zcm.monitor.service.alarm.utils.AlarmCache;
import com.ztesoft.zsmart.zcm.monitor.service.alarmchan.schedule.EmailScheduler;
import com.ztesoft.zsmart.zcm.monitor.service.panoramic.PanoramicAnnounceService;
import com.ztesoft.zsmart.zcm.monitor.service.panoramic.SchedulerTask;
import com.ztesoft.zsmart.zcm.monitor.service.selfmonitor.SelfMonitorAlarmFactory;
import com.ztesoft.zsmart.zcm.monitor.service.selfmonitor.impl.AlarmCountStatus;
import com.ztesoft.zsmart.zcm.monitor.service.selfmonitor.model.AlarmCountMapEnum;
import com.ztesoft.zsmart.zcm.monitor.service.selfmonitor.model.SelfMonitorStateEnum;
import com.ztesoft.zsmart.zcm.monitor.service.selfmonitor.model.SelfMonitorTypeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

/**
 * 定时任务配置类
 *
 * @Author FJJ
 * @Date 2020/1/9
 */
@Configuration
@EnableScheduling
public class ScheduledConfiguration {

    private static final ZSmartLogger logger = ZSmartLogger.getLogger(SchedulerTask.class);

    @Autowired
    private PanoramicAnnounceService announceService;

    @Autowired
    AlarmCountStatus alarmCountStatus;

    @Autowired
    EmailScheduler emailScheduler;


    @Scheduled(cron = "${zcm.monitor.checkalive.cron.expression:0 0 10 * * ?}")
    public void iMonitorEverydayScheduledTask() {

        iMonitorAliveNotificationTask();

        iMonitorDayAlarmCountResetTask();

    }


    /**
     * 0 0 1 * * ?每天凌晨1点清理alarmEvent数据库
     */
    @Scheduled(cron = "${zcm.monitor.clear-alarm-event.cron.expression:0 0 1 * * ?}")
    public void clearAlarmEventDataTask() {
        try {
            announceService.clearAlarmEventDataTask();
        }
        catch (Exception e) {
            logger.error(e.getMessage());
        }
    }

    /**
     * 0 0/5 * * * ?每5分钟执行smtp服务
     */
    @Scheduled(cron = "${zcm.monitor.open-smtp-email.cron.expression:0 0/5 * * * ?}")
    public void getEmailAndSendAlarm() {
        try {
            emailScheduler.getEmailAndSendAlarm();
        }
        catch (Exception e) {
            logger.error(e.getMessage());
        }
    }

    /**
     * 监控存活通知定时任务
     * 默认每天早上10点
     */
    private void iMonitorAliveNotificationTask() {

        AlarmEvent alarmEvent = SelfMonitorAlarmFactory.create(SelfMonitorTypeEnum.MONITOR_ALIVE_000);

        AlarmCache.offerAlarmEventToQueue(alarmEvent);

    }

    /**
     * 监控告警计数每日告警及清理
     * 默认每天早上10点
     */
    private void iMonitorDayAlarmCountResetTask() {

        // 先告警
        if (!alarmCountStatus.checkStatus().equals(SelfMonitorStateEnum.OK)) {
            AlarmEvent alarmEvent = SelfMonitorAlarmFactory.create(SelfMonitorTypeEnum.MONITOR_ALARM_COUNT_003);
            AlarmCache.offerAlarmEventToQueue(alarmEvent);
        }

        // 再清理
        for (AlarmCountMapEnum e : AlarmCountMapEnum.values()) {
            e.resetDayAlarmCount();
        }

    }

}
