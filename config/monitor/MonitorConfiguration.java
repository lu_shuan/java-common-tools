package com.ztesoft.zsmart.zcm.monitor.config;

import com.google.common.util.concurrent.RateLimiter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;

/**
 * monitor内部bean集合
 *
 * @Author FJJ
 * @Date 2019/12/12
 */
@Configuration
public class MonitorConfiguration {


    /**
     * 限流控制，暂定1秒钟100个
     */
    @Bean
    public RateLimiter createRateLimiter(MonitorProperties monitorProperties) {

        return RateLimiter.create(monitorProperties.getHttpAlarmLimitCount());

    }
    @Bean
    public ClassLoaderTemplateResolver secondaryTemplateResolver() {
        ClassLoaderTemplateResolver secondaryTemplateResolver = new ClassLoaderTemplateResolver();
        secondaryTemplateResolver.setPrefix("static/");
        secondaryTemplateResolver.setSuffix(".html");
        secondaryTemplateResolver.setTemplateMode(TemplateMode.HTML);
        secondaryTemplateResolver.setCharacterEncoding("UTF-8");
        secondaryTemplateResolver.setOrder(1);
        secondaryTemplateResolver.setCheckExistence(true);

        return secondaryTemplateResolver;
    }

}
