package com.ztesoft.zsmart.zcm.monitor.config;

import com.ztesoft.zsmart.core.utils.DateUtil;
import org.apache.shardingsphere.api.sharding.standard.PreciseShardingAlgorithm;
import org.apache.shardingsphere.api.sharding.standard.PreciseShardingValue;

import java.util.Collection;
import java.util.Date;

public  class NmsPreciseShardingAlgorithm implements PreciseShardingAlgorithm<Date> {
    @Override
    public String doSharding(Collection<String> collection, PreciseShardingValue<Date> preciseShardingValue) {
        StringBuffer tableName = new StringBuffer();
        tableName.append(preciseShardingValue.getLogicTableName()).append("_")
                .append(DateUtil.date2String(preciseShardingValue.getValue(), "yyyyMM"));
        return tableName.toString();
    }
}
