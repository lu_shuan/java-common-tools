package com.ztesoft.zsmart.zcm.monitor.config;

import lombok.Data;

@Data
public class RedisClusterConfig {
    String ip;
    Integer port;
}
