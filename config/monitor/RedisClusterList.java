package com.ztesoft.zsmart.zcm.monitor.config;

import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import lombok.Data;

@Configuration
@ConfigurationProperties(prefix = "redis.cluster")
@Data
public class RedisClusterList {

    List<RedisClusterConfig> list = new ArrayList<>();
}
