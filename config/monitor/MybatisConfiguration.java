package com.ztesoft.zsmart.zcm.monitor.config;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.ztesoft.zsmart.core.jdbc.mybatis.CoreSqlSessionFactoryBean;
import com.ztesoft.zsmart.zcm.monitor.filter.DynamicSqlInjectionFilter;
import org.springframework.context.annotation.Primary;

/**
 * Created by lkj on 2018/4/09.
 */
@Configuration
@MapperScan(basePackages = {
    "com.ztesoft.zsmart.zcm.monitor.mapper.sharding"
}, sqlSessionFactoryRef = "sqlSessionFactoryBean")

public class MybatisConfiguration {
     @Resource(name = "shardingDataSource")
     @Qualifier("shardingDataSource")
     DataSource dataSource;

    /**
     * Core sql session factory bean.
     *
     * @param dataSource the data source
     * @return the sql session factory bean
     */
    @Bean("sqlSessionFactoryBean")
    @Primary
    public SqlSessionFactoryBean coreSqlSessionFactoryBean(@Qualifier("shardingDataSource") DataSource dataSource) {

        // CoreSqlSessionFactoryBean sqlSessionFactoryBean = new CoreSqlSessionFactoryBean();
        //
        // sqlSessionFactoryBean.setDataSource(dataSource);
        //
        // return sqlSessionFactoryBean;

        // CoreSqlSessionFactoryBean ssfb = new CoreSqlSessionFactoryBean();
        CoreSqlSessionFactoryBean ssfb = new CoreSqlSessionFactoryBean();
        ssfb.setDataSource(dataSource);
        ssfb.setTypeAliases(new Class[] {
            DynamicSqlInjectionFilter.class
        });
        // ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
        // try {
        // ssfb.setMapperLocations(resolver.getResources("classpath:com/ztesoft/zsmart/zcm/monitor/**/nmsdb/**.xml"));
        // }
        // catch (IOException e) {
        // }
        return ssfb;

    }

}
