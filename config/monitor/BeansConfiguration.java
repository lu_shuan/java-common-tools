package com.ztesoft.zsmart.zcm.monitor.config;

import com.ztesoft.zsmart.core.exception.BaseAppException;
import com.ztesoft.zsmart.core.jdbc.mybatis.session.CoreSqlSessionTemplate;
import com.ztesoft.zsmart.zcm.core.exception.ExceptionPublisher;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeansConfiguration {

    @Bean
    public CoreSqlSessionTemplate coreSqlSessionTemplate(
            @Qualifier("sqlSessionFactoryBean") SqlSessionFactoryBean readCoreSqlSessionFactoryBean)
            throws BaseAppException {
        CoreSqlSessionTemplate coreSqlSessionTemplate = null;
        try {
            coreSqlSessionTemplate = new CoreSqlSessionTemplate(readCoreSqlSessionFactoryBean.getObject());
        }
        catch (Exception e) {
            ExceptionPublisher.publish(e, "");
        }
        return coreSqlSessionTemplate;
    }

}
