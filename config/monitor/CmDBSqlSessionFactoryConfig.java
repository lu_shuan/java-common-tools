package com.ztesoft.zsmart.zcm.monitor.config;

import java.io.IOException;
import javax.sql.DataSource;

import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;

import com.ztesoft.zsmart.core.log.ZSmartLogger;

/**
 * TODO
 *
 * @Author FJJ
 * @Date 2018/7/4
 */
@Configuration
@AutoConfigureAfter(MybatisConfiguration.class)

@MapperScan(basePackages = "com.ztesoft.zsmart.zcm.monitor.mapper.cmdb", sqlSessionFactoryRef = "cmDBSqlSessionFactory")
public class CmDBSqlSessionFactoryConfig {
    
    /** The logger. */
    private ZSmartLogger logger = ZSmartLogger.getLogger(CmDBSqlSessionFactoryConfig.class);

    /**
     * Sql session factory.
     *
     * @param dataSource the data source
     * @return the sql session factory bean
     */
    @Bean(name = "cmDBSqlSessionFactory")
    SqlSessionFactoryBean sqlSessionFactory(@Qualifier("cmdbDataSource") DataSource dataSource) {
        SqlSessionFactoryBean ssfb = new SqlSessionFactoryBean();
        ssfb.setDataSource(dataSource);
        ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();

        // //分页插件
        // PaginationInterceptor pageHelper = new PaginationInterceptor();
        // //添加插件
        // ssfb.setPlugins(new Interceptor[] {
        // (Interceptor) pageHelper
        // });

        try {
            ssfb.setMapperLocations(resolver.getResources("classpath:com/ztesoft/zsmart/zcm/monitor/**/cmdb/**.xml"));
        }
        catch (IOException e) {
            logger.error(e.getMessage(), e);
        }
        return ssfb;
    }
    //
    // @Bean(name = "cmDBTransactionManager")
    // PlatformTransactionManager transactionManager(@Qualifier("cmdbDataSource")DataSource dataSource) {
    // DataSourceTransactionManager transactionManager = new DataSourceTransactionManager();
    // transactionManager.setDataSource(dataSource);
    // return transactionManager;
    // }

}
