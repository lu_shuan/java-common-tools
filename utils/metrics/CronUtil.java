package com.ztesoft.zsmart.zcm.metrics.utils;

/**
 * Created by Administrator on 07/18/18 018.
 */
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Java日期转换成Cron日期表达式工具类
 * 
 * @author Admin
 */
public final class CronUtil {

    private CronUtil() {

    }

    /***
     * @param date
     * @param dateFormat : e.g:yyyy-MM-dd HH:mm:ss
     * @return
     */
    public static String formatDateByPattern(Date date, String dateFormat) {
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
        String formatTimeStr = null;
        if (date != null) {
            formatTimeStr = sdf.format(date);
        }
        return formatTimeStr;
    }

    /***
     * convert Date to cron ,eg. "0 06 10 15 1 ? 2014"
     * 
     * @param date : 时间点
     * @return
     */
    public static String getCron(Date date) {
        String dateFormat = "ss mm HH dd MM ? yyyy";
        return formatDateByPattern(date, dateFormat);
    }

}