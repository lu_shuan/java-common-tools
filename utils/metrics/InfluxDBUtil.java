
//
//    /**
//     * 写入数据库
//     *
//     * @param db 数据库名
//     * @param retention 保存策略
//     * @param measurement 表
//     * @param time 时间
//     * @param tags 标签
//     * @param fields 值
//     */
//    public static void write(String db, String retention, String measurement, long time, Map<String, String> tags,
//        Map<String, Object> fields) {
//
//        try {
//            // 保存到influxdb
//            Point point = Point.measurement(measurement).time(time, TimeUnit.MILLISECONDS).tag(tags).fields(fields)
//                .build();
//
//            InfluxDBClient.getInstance().write(db, retention, point);
//        }
//        catch (Exception e) {
//            logger.error(e);
//        }
//
//    }
//
//    /**
//     * 写入数据库
//     *
//     * @param db 数据库名
//     * @param retention 保存策略
//     * @param measurement 表
//     * @param time 时间
//     * @param timeUnit 时间类型
//     * @param tags 标签
//     * @param fields 值
//     */
//    public static void write(String db, String retention, String measurement, long time, TimeUnit timeUnit,
//        Map<String, String> tags, Map<String, Object> fields) {
//
//        try {
//            // 保存到influxdb
//            Point point = Point.measurement(measurement).time(time, timeUnit).tag(tags).fields(fields).build();
//
//            InfluxDBClient.getInstance().write(db, retention, point);
//        }
//        catch (Exception e) {
//            logger.error(e);
//        }
//
//    }
//

package com.ztesoft.zsmart.zcm.metrics.utils;

import com.ztesoft.zsmart.core.log.ZSmartLogger;
import com.ztesoft.zsmart.zcm.metrics.config.MetricsStorageProperties;
import org.influxdb.InfluxDB;
import org.influxdb.InfluxDBFactory;
import org.influxdb.dto.BatchPoints;
import org.influxdb.dto.Point;

/**
 * Created by lkj on 2018/4/09.
 */
public final class InfluxDBUtil {

    private static final ZSmartLogger logger = ZSmartLogger.getLogger(InfluxDBUtil.class);

    private InfluxDBUtil() {
    }

    /**
     * 单例模式
     */
    private static InfluxDB influxDB;

    /**
     * 获取influxDB
     * 
     * @return InfluxDB
     */
    public static InfluxDB getInfluxDB() {
        if (influxDB == null) {
            synchronized (InfluxDBUtil.class) {
                if (null != influxDB) {
                    return influxDB;
                }
                MetricsStorageProperties imonitorProperties = SpringContextUtil.getBean(MetricsStorageProperties.class);
                String influxDBUrl = imonitorProperties.getInfluxdbUrl();
                String influxDBUser = imonitorProperties.getInfluxdbUser();
                String influxDBPassword = imonitorProperties.getInfluxdbPassword();
                logger.info("Influxdb url is [{}], access user is [{}], password is [{}]", influxDBUrl, influxDBUser,
                    influxDBPassword);
                if (influxDBUser != null && !"".equals(influxDBUser) && influxDBPassword != null
                    && !"".equals(influxDBPassword)) {
                    influxDB = InfluxDBFactory.connect(influxDBUrl, influxDBUser, influxDBPassword);
                }
                else {
                    influxDB = InfluxDBFactory.connect(influxDBUrl);
                }
            }
        }
        return influxDB;
    }

    /**
     * 批量写
     *
     * @param batchPoints BatchPoints
     */
    public static void batchWrite(BatchPoints batchPoints) {
        getInfluxDB().write(batchPoints);
    }

    /**
     * 单点写
     *
     * @param db String
     * @param retention String
     * @param point Point
     */
    public static void write(String db, String retention, Point point) {
        getInfluxDB().write(db, retention, point);
    }

    public static void main(String[] args) {
        /**
         * InfluxDB influxdb = InfluxDBFactory.connect("http://172.16.17.82:8586", "admin", "abc@123A"); Query query =
         * new Query("select * from alarm_event limit 100", "alarm"); QueryResult result = influxdb.query(query);
         */
    }
}
