package com.ztesoft.zsmart.zcm.metrics.utils;

import com.ztesoft.zsmart.core.log.ZSmartLogger;
import com.ztesoft.zsmart.zcm.metrics.config.MetricsProperties;
import com.ztesoft.zsmart.zcm.metrics.config.MetricsStorageProperties;
import org.apache.commons.lang3.StringUtils;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.HashMap;

/**
 * Created by lkj on 2018/4/10.
 */
public final class RedisUtil {
    private static final ZSmartLogger logger = ZSmartLogger.getLogger(RedisUtil.class);

    private static final int REDIS_DEFAULT_PORT = 6379;

    private static final int DEFAULT_TIME_OUT = 2000;

    private static final int REDIS_DEFAULT_DATABASE = 0;

    public static final int KEY_EXPIRE_TIME = ((MetricsProperties) SpringContextUtil.getBean(MetricsProperties.class))
        .getJedisKeyExpireTime();

    public static final int LIMIT_KEY_EXPIRE_TIME = ((MetricsProperties) SpringContextUtil
        .getBean(MetricsProperties.class)).getCpuLimitJedisKeyExpireTime();
    public static final int COLLECTD_KEY_EXPIRE_TIME = ((MetricsProperties) SpringContextUtil.getBean(MetricsProperties.class))
            .getCollectdJedisKeyExpireTime();

    public static final int COLLECTD_KPI_KEY_EXPIRE_TIME = ((MetricsProperties) SpringContextUtil
        .getBean(MetricsProperties.class)).getCollectdKpiJedisKeyExpireTime();

    public static final int NODE_KEY_EXPIRE_TIME = ((MetricsProperties) SpringContextUtil
        .getBean(MetricsProperties.class)).getNodeJedisKeyExpireTime();

    public static final int NODE_KPI_KEY_EXPIRE_TIME = ((MetricsProperties) SpringContextUtil
        .getBean(MetricsProperties.class)).getNodeKpiJedisKeyExpireTime();

    public static final String CHARSET = "utf-8";

    // JedisPool
    private static volatile JedisPool jedisPool;

    private RedisUtil() {
    }

    /**
     * JedisPool
     *
     * @return JedisPool
     */
    private static JedisPool getJedisPool() {
        if (jedisPool == null) {
            synchronized (RedisUtil.class) {
                if (jedisPool == null) {
                    MetricsStorageProperties metricsStorageProperties = SpringContextUtil
                        .getBean(MetricsStorageProperties.class);
                    JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
                    // 最大分配的对象数
                    jedisPoolConfig.setMaxTotal(1024);
                    // 最大能够保持idel状态的对象数
                    jedisPoolConfig.setMaxIdle(200);
                    // 当池内没有返回对象时，最大等待时间
                    jedisPoolConfig.setMaxWaitMillis(1000);
                    // jedisPool = new JedisPool(jedisPoolConfig, "10.45.80.3",6379,2000);
                    // logger.info("redis url is [http://{}:{}]", "10.45.80.3","6379");
                    //
                    // String host = "10.45.80.3";
                    // int port = 6379;
                    //
                    // jedisPoolConfig.setMaxWaitMillis(1000);
                    // jedisPool = new JedisPool(jedisPoolConfig, "10.45.80.3",6379,2000, null, 0);

                    // jedisPool = new JedisPool(jedisPoolConfig, metricsStorageProperties.getRedisHost(),
                    // StringUtils.isEmpty(metricsStorageProperties.getRedisPort())
                    // ? REDIS_DEFAULT_PORT : Integer.parseInt(metricsStorageProperties.getRedisPort()),
                    // DEFAULT_TIME_OUT, metricsStorageProperties.getRedisPassword(),
                    // StringUtils.isEmpty(metricsStorageProperties.getRedisDatabase())
                    // ? REDIS_DEFAULT_DATABASE : Integer.parseInt(metricsStorageProperties.getRedisDatabase()));

                    jedisPool = new JedisPool(jedisPoolConfig, metricsStorageProperties.getRedisHost(),
                        StringUtils.isBlank(metricsStorageProperties.getRedisPort()) ? REDIS_DEFAULT_PORT
                            : Integer.parseInt(metricsStorageProperties.getRedisPort()),
                        DEFAULT_TIME_OUT,
                        StringUtils.isBlank(metricsStorageProperties.getRedisPassword()) ? null
                            : metricsStorageProperties.getRedisPassword(),
                        StringUtils.isBlank(metricsStorageProperties.getRedisDatabase()) ? REDIS_DEFAULT_DATABASE
                            : Integer.parseInt(metricsStorageProperties.getRedisDatabase()));

                    logger.info("redis url is [http://{}:{}], database index is [{}]",
                        metricsStorageProperties.getRedisHost(),
                        StringUtils.isBlank(metricsStorageProperties.getRedisPort()) ? REDIS_DEFAULT_PORT
                            : metricsStorageProperties.getRedisPort(),
                        StringUtils.isBlank(metricsStorageProperties.getRedisDatabase()) ? REDIS_DEFAULT_DATABASE
                            : metricsStorageProperties.getRedisDatabase());
                }
            }
        }
        return jedisPool;
    }

    public static JedisPool createUserRedisPool(String host, int port, String password, int dbIndex) {

        if (jedisPool == null) {
            synchronized (RedisUtil.class) {
                if (jedisPool == null) {
                    JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
                    // 最大分配的对象数
                    jedisPoolConfig.setMaxTotal(1024);
                    // 最大能够保持idel状态的对象数
                    jedisPoolConfig.setMaxIdle(200);
                    // 当池内没有返回对象时，最大等待时间
                    jedisPoolConfig.setMaxWaitMillis(1000);
                    jedisPool = new JedisPool(jedisPoolConfig, host, port, 2000, password, dbIndex);

                    logger.info("redis url is [http://[{}]:[{}]]", host, port);
                }
            }
        }
        return jedisPool;
    }

    /**
     * 从jedis pool 返回一个jedis客户端
     *
     * @return Jedis
     */
    public static Jedis getJedis() {
        return getJedisPool().getResource();
    }

    /**
     * hget
     *
     * @param key key
     * @param field field
     */
    public static <T> T hget(String key, String field, Class<T> clazz) {
        Jedis jedis = null;
        try {
            jedis = getJedis();
            byte[] result = jedis.hget(key.getBytes("UTF-8"), field.getBytes("UTF-8"));
            return result != null ? (SerializeUtil.unserialize(result, clazz)) : null;
        }
        catch (Exception e) {
            logger.error(e);
        }
        finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        return null;
    }

    /**
     * hset
     *
     * @param key key
     * @param field field
     * @param t t
     */
    public static <T> void hset(String key, String field, T t) {
        Jedis jedis = null;
        try {
            jedis = getJedis();
            jedis.hset(key.getBytes("UTF-8"), field.getBytes("UTF-8"), SerializeUtil.serialize(t));
        }
        catch (Exception e) {
            logger.error(e);
        }
        finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    /**
     * 设置字符串
     *
     * @param key String
     * @param value String
     */
    public static void set(String key, String value) {
        long beginTime = System.currentTimeMillis();
        Jedis jedis = getJedis();
        try {
            jedis.set(key, value);
        }
        catch (Exception e) {
            logger.error(e);
        }
        finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        SelfMonitorCache.setRedisDuration.getAndAdd(System.currentTimeMillis() - beginTime);
    }

    /**
     * 设置字符串,同时设置过期时间
     * 
     * @param key key
     * @param value value
     * @param nxxx 设置条件，nx：不存在时才设置 xx：存在时才设置
     * @param expx 时间单位，ex: 秒 px：毫秒
     * @param time 过期时间
     */
    public static String set(String key, String value, String nxxx, String expx, int time) {
        long beginTime = System.currentTimeMillis();
        String result = null;
        Jedis jedis = getJedis();
        try {
            result = jedis.set(key, value);
            jedis.expire(key, time);
        }
        catch (Exception e) {
            logger.error(e);
        }
        finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        SelfMonitorCache.setRedisDuration.getAndAdd(System.currentTimeMillis() - beginTime);
        return result;

    }

    /**
     * 获取字符串
     *
     * @param key String
     * @return String
     */
    public static String get(String key) {
        long beginTime = System.currentTimeMillis();
        Jedis jedis = getJedis();
        try {
            return jedis.get(key);
        }
        catch (Exception e) {
            logger.error(e);
        }
        finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        SelfMonitorCache.getRedisDuration.getAndAdd(System.currentTimeMillis() - beginTime);
        return null;
    }

    /**
     * 删除字符串
     *
     * @param key String
     */
    public static void del(String key) {
        long beginTime = System.currentTimeMillis();
        Jedis jedis = getJedis();
        try {
            jedis.del(key);
        }
        catch (Exception e) {
            logger.error(e);
        }
        finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        SelfMonitorCache.delRedisDuration.getAndAdd(System.currentTimeMillis() - beginTime);
    }

    /**
     * 添加集合元素
     *
     * @param key byte[]
     * @param mem byte[]
     */
    public static void sadd(byte[] key, byte[] mem) {
        Jedis jedis = getJedis();
        try {
            jedis.sadd(key, mem);
        }
        catch (Exception e) {
            logger.error(e);
        }
        finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    /**
     * 删除集合元素
     *
     * @param key byte[]
     * @param mem byte[]
     */
    public static void srem(byte[] key, byte[] mem) {
        Jedis jedis = getJedis();
        try {
            jedis.srem(key, mem);
        }
        catch (Exception e) {
            logger.error(e);
        }
        finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    /**
     * 获取全部集合元素
     *
     * @param key byte[]
     * @return Set
     */
    public static Set<byte[]> smembers(byte[] key) {
        Jedis jedis = getJedis();
        try {
            return jedis.smembers(key);
        }
        catch (Exception e) {
            logger.error(e);
        }
        finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        return new HashSet<>();
    }

    /**
     * 添加哈希，设置生存周期
     *
     * @param key byte[]
     * @param field byte[]
     * @param value byte[]
     */
    public static void hsetByTTL(byte[] key, byte[] field, byte[] value) {
        Jedis jedis = getJedis();
        try {
            jedis.hset(key, field, value);
            jedis.expire(key, 6 * 60 * 60);
        }
        catch (Exception e) {
            logger.error(e);
        }
        finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    /**
     * 添加哈希,存在则不保存
     *
     * @param key byte[]
     * @param field byte[]
     * @param value byte[]
     */
    public static void hsetIfNotExists(byte[] key, byte[] field, byte[] value) {
        Jedis jedis = getJedis();
        try {
            if (!jedis.hexists(key, field)) {
                jedis.hset(key, field, value);
            }
        }
        catch (Exception e) {
            logger.error(e);
        }
        finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    /**
     * 添加哈希
     *
     * @param key byte[]
     * @param field byte[]
     * @param value byte[]
     */
    public static void hset(byte[] key, byte[] field, byte[] value) {
        Jedis jedis = getJedis();
        try {
            jedis.hset(key, field, value);
        }
        catch (Exception e) {
            logger.error(e);
        }
        finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    public static void hset(String key, String field, String value) {
        Jedis jedis = getJedis();
        try {
            jedis.hset(key, field, value);
        }
        catch (Exception e) {
            logger.error(e);
        }
        finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    public static void expire(byte[] key, int time) {
        Jedis jedis = getJedis();
        try {
            jedis.expire(key, time);
        }
        catch (Exception e) {
            logger.error(e);
        }
        finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    public static void expire(String key, int time) {
        Jedis jedis = getJedis();
        try {
            jedis.expire(key, time);
        }
        catch (Exception e) {
            logger.error(e);
        }
        finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    /**
     * 删除哈希
     *
     * @param key byte[]
     * @param field byte[]
     */
    public static void hdel(byte[] key, byte[] field) {
        Jedis jedis = getJedis();
        try {
            jedis.hdel(key, field);
        }
        catch (Exception e) {
            logger.error(e);
        }
        finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    /**
     * 获取全部哈希值
     *
     * @param key byte[]
     * @return List<byte[]>
     */
    public static List<byte[]> hvals(byte[] key) {
        Jedis jedis = getJedis();
        try {
            return jedis.hvals(key);
        }
        catch (Exception e) {
            logger.error(e);
        }
        finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        return new ArrayList<>();
    }

    /**
     * 获取哈希值
     *
     * @param key byte[]
     * @param field byte[]
     * @return byte[]
     */
    public static byte[] hget(byte[] key, byte[] field) {
        Jedis jedis = getJedis();
        try {
            return jedis.hget(key, field);
        }
        catch (Exception e) {
            logger.error(e);
        }
        finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        return null;
    }

    /**
     * 获取哈希值
     *
     * @param key byte[]
     * @return byte[]
     */
    public static Map<byte[], byte[]> hgetAllByte(byte[] key) {
        Jedis jedis = getJedis();
        try {
            return jedis.hgetAll(key);
        }
        catch (Exception e) {
            logger.error(e);
        }
        finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        return null;
    }

    /**
     * 获取哈希值
     *
     * @param key byte[]
     * @return byte[]
     */
    public static Map<String, String> hgetAllString(String key) {
        Jedis jedis = getJedis();
        try {
            return jedis.hgetAll(key);
        }
        catch (Exception e) {
            logger.error(e);
        }
        finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        return null;
    }

    /**
     * 获取keys
     *
     * @param pattern String
     * @return Set<String>
     */
    public static Set<String> keys(String pattern) {
        Jedis jedis = getJedis();
        try {
            return jedis.keys(pattern);
        }
        catch (Exception e) {
            logger.error(e);
        }
        finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        return new HashSet<>();
    }

    /**
     * 批量设置生命周期
     *
     * @param pattern String
     * @param seconds ttl int
     */
    public static void batchExpire(String pattern, int seconds) {
        Jedis jedis = getJedis();
        try {
            Set<String> keys = jedis.keys(pattern);
            Iterator<String> keysIterator = keys.iterator();
            while (keysIterator.hasNext()) {
                String key = keysIterator.next();
                if (seconds != 0 && jedis.exists(key) && jedis.ttl(key) == -1) {
                    jedis.expire(key, seconds);
                }
            }
        }
        catch (Exception e) {
            logger.error(e);
        }
        finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    public static void closePool() {

        JedisPool pool = getJedisPool();
        if (pool != null) {
            pool.close();
        }

    }

    /**
     * Redis Hget 命令用于返回哈希表中指定字段的值。
     *
     * @param key
     * @return 返回给定字段的值。如果给定的字段或 key 不存在时，返回 nil
     */
    public static Map<byte[], byte[]> hgetByExpireTime(byte[] key) {
        long beginTime = System.currentTimeMillis();
        Jedis jedis = getJedis();
        try {
            Map<byte[], byte[]> all = jedis.hgetAll(key);
            return all;
        }
        catch (Exception e) {
            logger.error(e);
        }
        finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        SelfMonitorCache.getRedisDuration.getAndAdd(System.currentTimeMillis() - beginTime);
        return new HashMap<>();
    }

    /**
     * 获取字符串
     *
     * @param key String
     * @return String
     */
    public static byte[] getByExpireTime(byte[] key) {
        long beginTime = System.currentTimeMillis();
        Jedis jedis = getJedis();
        try {
            return jedis.get(key);
        }
        catch (Exception e) {
            logger.error(e);
        }
        finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        SelfMonitorCache.getRedisDuration.getAndAdd(System.currentTimeMillis() - beginTime);
        return null;
    }

    /**
     * 删除字符串
     *
     * @param key String
     */
    public static void delByExpireTime(byte[] key) {
        long beginTime = System.currentTimeMillis();
        Jedis jedis = getJedis();
        try {
            jedis.del(key);
        }
        catch (Exception e) {
            logger.error(e);
        }
        finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        SelfMonitorCache.delRedisDuration.getAndAdd(System.currentTimeMillis() - beginTime);
    }

    // /**
    // * Redis Rpush 命令用于将一个或多个值插入到列表的尾部(最右边)。如果列表不存在，一个空列表会被创建并执行 RPUSH 操作。 当列表存在但不是列表类型时，返回一个错误。 在 Redis 2.4 版本以前的
    // RPUSH
    // * 命令，都只接受单个 value 值。
    // *
    // * @param key
    // * @param vals
    // * @return 执行 RPUSH 操作后，列表的长度。
    // */
    // public static long rpushByExpireTime(String key, String... vals) {
    // Jedis jedis = getJedis();
    // long num = 0;
    // num = rpushAndExpire(key, jedis, num, KEY_EXPIRE_TIME, vals);
    // return num;
    // }
    //
    // public static long rpushKpiByExpireTime(String key, String... vals) {
    // Jedis jedis = getJedis();
    // long num = 0;
    // num = rpushAndExpire(key, jedis, num, KPI_KEY_EXPIRE_TIME, vals);
    // return num;
    // }

    public static long rpushAndExpire(String key, Jedis jedis, long num, int keyExpireTime, String[] vals) {
        long beginTime = System.currentTimeMillis();
        try {
            num = jedis.rpush(key, vals);
            jedis.expire(key, keyExpireTime);
        }
        catch (Exception e) {
            logger.error(e);
        }
        finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        SelfMonitorCache.rpushRedisDuration.getAndAdd(System.currentTimeMillis() - beginTime);
        return num;
    }

    /**
     * Redis Rpop 命令用于移除列表的最后一个元素，返回值为移除的元素。
     *
     * @param key
     * @return 被移除的元素。当列表不存在时，返回 nil 。
     */
    public static byte[] rpopByExpireTime(byte[] key) {
        long beginTime = System.currentTimeMillis();
        Jedis jedis = getJedis();
        try {
            byte[] val = jedis.rpop(key);
            return val;
        }
        catch (Exception e) {
            logger.error(e);
        }
        finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        SelfMonitorCache.rpopRedisDuration.getAndAdd(System.currentTimeMillis() - beginTime);
        return null;
    }

    public static String rpopByExpireTime(String key) {
        long beginTime = System.currentTimeMillis();
        Jedis jedis = getJedis();
        try {
            String val = jedis.rpop(key);
            return val;
        }
        catch (Exception e) {
            logger.error(e);
        }
        finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        SelfMonitorCache.rpopRedisDuration.getAndAdd(System.currentTimeMillis() - beginTime);
        return null;
    }

    /**
     * Redis Lrange 返回列表中指定区间内的元素，区间以偏移量 START 和 END 指定。 其中 0 表示列表的第一个元素，1 表示列表的第二个元素，以此类推。 你也可以使用负数下标， 以 -1表示列表的最后一个元素，
     * -2 表示列表的倒数第二个元素，以此类推。
     *
     * @param key
     * @param start
     * @param end
     * @return 一个列表，包含指定区间内的元素。
     */
    public static List<byte[]> lrangeByExpireTime(byte[] key, long start, long end) {
        Jedis jedis = getJedis();
        try {
            List<byte[]> vals = jedis.lrange(key, start, end);
            jedis.del(key);
            return vals;
        }
        catch (Exception e) {
            logger.error(e);
        }
        finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        return null;
    }

    public static List<String> lrangeByExpireTime(String key, long start, long end) {
        Jedis jedis = getJedis();
        try {
            List<String> vals = jedis.lrange(key, start, end);
            jedis.del(key);
            return vals;
        }
        catch (Exception e) {
            logger.error(e);
        }
        finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        return null;
    }

    /**
     * Redis Hexists 命令用于查看哈希表的指定字段是否存在。
     *
     * @param key
     * @return 如果哈希表含有给定字段，返回 1 。 如果哈希表不含有给定字段，或 key 不存在，返回 0 。
     */
    public static boolean hexists(byte[] key) {
        long beginTime = System.currentTimeMillis();
        Jedis jedis = getJedis();
        boolean flag = false;
        try {
            flag = jedis.exists(key);
        }
        catch (Exception e) {
            logger.error(e);
        }
        finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        SelfMonitorCache.existsRedisDuration.getAndAdd(System.currentTimeMillis() - beginTime);
        return flag;
    }
}
