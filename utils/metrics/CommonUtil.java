package com.ztesoft.zsmart.zcm.metrics.utils;

import com.ztesoft.zsmart.core.log.ZSmartLogger;
import com.ztesoft.zsmart.zcm.metrics.model.nms.ResKpiDto;
import com.ztesoft.zsmart.zcm.metrics.model.nms.ZmcConst;
import org.springframework.core.env.Environment;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.TimeZone;

/**
 * 通用处理类，存放一些公共方法
 *
 * @Author FJJ
 * @Date 2018/7/23
 */
public final class CommonUtil {

    private static final ZSmartLogger logger = ZSmartLogger.getLogger(CommonUtil.class);

    private CommonUtil() {

    }

    /**
     * @param dto kpi配置表dto
     * @param colName 列名
     * @param value 列值
     * @param tagMap tagMap
     * @param fieldMap fieldMap
     * @return long型指标时间
     */
    public static long fillTagAndFieldWithKpiCfgDto(ResKpiDto dto, String colName, String value,
        Map<String, String> tagMap, Map<String, Object> fieldMap) {

        long kpiDate = 0L;

        // tag：T
        if (dto.getTagField().equals(ZmcConst.KPI_TAG_FALG)) {
            tagMap.put(colName, value);
        }
        // field：F
        else if (dto.getTagField().equals(ZmcConst.KPI_FIELD_FALG)) {
            fillSingleFieldValue(dto, colName, value, fieldMap);
        }
        // time series：D， 时序规定为utc标准时间规范，先精确到秒
        else if (dto.getTagField().equals(ZmcConst.KPI_TIME_SERIES_FALG)) {

            try {
                kpiDate = formatUTCTimeStr(value.substring(0, 19) + "Z").getTime();
            }
            catch (Exception e) {
                // 如果不按照规范格式来，又要非要配置成D，那就提示一下是哪个kpi
                logger.error("Parse UTC Time error, value:[{}], table:[{}], kpi:[{}] ", value, dto.getTableName(),
                    dto.getKpiCode());
                kpiDate = 0L;
            }
        }

        return kpiDate;

    }

    /**
     * 填充单个field，要区分下值类型
     *
     * @param dto 配置记录
     * @param colName 列名
     * @param value 值
     * @param fieldMap field集合
     */
    private static void fillSingleFieldValue(ResKpiDto dto, String colName, String value,
        Map<String, Object> fieldMap) {

        // 数值型
        if (dto.getValueType().equals(ZmcConst.KPI_FIELD_VALUE_TYPE_INTEGER)) {
            // 先全部按double型转
            if (value.contains(".")) {
                double dValue = Double.valueOf(value);
                dValue = (double) Math.round(dValue * 100) / 100;
                fieldMap.put(colName, dValue);
            }
            else {
                fieldMap.put(colName, Double.valueOf(value));
            }

        }
        // String型
        else {
            fieldMap.put(colName, value);
        }

    }

    /**
     * 从环境变量中获取参数值，没有就使用默认值
     *
     * @param env spring环境变量
     * @param paramName 参数名
     * @param defaultValue 默认值
     * @return 参数值
     */
    public static String getEnvParamWithDefaultValue(Environment env, String paramName, String defaultValue) {

        String paramValue = env.getProperty(paramName);
        if (paramValue == null || "".equals(paramValue)) {
            paramValue = defaultValue;
        }
        return paramValue;
    }

    /**
     * 将utc字符串时间转化为当前时间
     *
     * @param utcTime UTC
     * @return Date
     */
    public static Date formatUTCTimeStr(String utcTime) {
        String timeStrFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'";
        SimpleDateFormat dateDF = new SimpleDateFormat(timeStrFormat);
        dateDF.setTimeZone(TimeZone.getTimeZone("UTC"));
        try {
            return dateDF.parse(utcTime);
        }
        catch (ParseException e) {
            return new Date();
        }
    }
}
