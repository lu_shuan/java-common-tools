package com.ztesoft.zsmart.zcm.metrics.utils;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by li.peilong on 2018/12/24.
 */
public class FormatUtil {
    
    /**
     * 对浮点数进行格式化，主要用于指定浮点数的精度
     *
     * @param value
     * @param scale 保留几位小数点
     * @return
     */
    public static Object formatDecimal(Object value, int scale) {
        Object result = value;
        if (value instanceof Float) {
            BigDecimal bg = new BigDecimal((Float) value);
            result = bg.setScale(scale, BigDecimal.ROUND_HALF_UP).floatValue();
        }
        else if (value instanceof Double) {
            BigDecimal bg = new BigDecimal((Double) value);
            result = bg.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
        }
        return result;
    }

    /**
     * 对数据进行格式化，浮点数仅保留两位小数
     *
     * @param fields
     * @return
     */
    public static  Map<String, Object> format(Map<String, Object> fields) {
        Map<String, Object> formatedFields = new HashMap<>();

        if (fields == null || fields.isEmpty()) {
            return formatedFields;
        }
        for (Map.Entry<String, Object> entry : fields.entrySet()) {
            if (entry.getValue() instanceof Float) {
                BigDecimal bg = new BigDecimal((Float) entry.getValue());
                Float value = bg.setScale(2, BigDecimal.ROUND_HALF_UP).floatValue();
                formatedFields.put(entry.getKey(), value);
            }
            else if (entry.getValue() instanceof Double) {
                BigDecimal bg = new BigDecimal((Double) entry.getValue());
                Double value = bg.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
                formatedFields.put(entry.getKey(), value);
            }
            else if (entry.getValue() instanceof BigDecimal) {
                BigDecimal bg = (BigDecimal) entry.getValue();
                Double value = bg.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
                formatedFields.put(entry.getKey(), value);
            }
            else {
                formatedFields.put(entry.getKey(), entry.getValue());
            }
        }
        return formatedFields;
    }

    public static Object formatDecimal(Object value) {
        return formatDecimal(value, 2);
    }
}
