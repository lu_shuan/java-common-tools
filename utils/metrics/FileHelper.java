package com.ztesoft.zsmart.zcm.metrics.utils;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

public final class FileHelper {

    private FileHelper() {

    }

    /**
     * 根据文件名读取返回字符串
     * 
     * @throws IOException
     */
    public static String readFile(String fileName) throws IOException {
        String content = "";
        content = FileUtils.readFileToString(new File(fileName));

        return content;
    }

}
