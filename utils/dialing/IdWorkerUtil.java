package com.ztesoft.zsmart.zcm.dialing.util;

/**
 * @Author xuduan
 * @Date 2020/12/15 0015 16:32
 */
public class IdWorkerUtil {

    private static IdWorker idWorker;

    static {
        idWorker = new IdWorker();
    }

    public static String getNextId() {
        return idWorker.nextId().toString();
    }

    public String getName() {
        return this.getClass().toString();
    }
}
