package com.ztesoft.zsmart.zcm.dialing.util;

import java.util.Locale;

/**
 * 操作系统信息类
 */
public final class OSUtil {

    private static final String osName = System.getProperty("os.name").toUpperCase(Locale.ENGLISH);

    /**
     * 判断操作系统类型
     */
    public static boolean isWindows() {
        return osName.contains("WINDOWS");
    }

    public static boolean isSun() {
        return osName.contains("SUNOS");
    }

    public String getName() {
        return this.getClass().toString();
    }

}
