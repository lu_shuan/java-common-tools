package com.ztesoft.zsmart.zcm.dialing.util.scriptUtils;

import java.io.InputStream;
import java.io.InputStreamReader;

import com.ztesoft.zsmart.core.log.ZSmartLogger;
import org.apache.commons.io.IOUtils;



public class StreamReader extends Thread {
    private static ZSmartLogger logger = ZSmartLogger.getLogger(StreamReader.class);

    InputStream in = null;

    StringBuffer errStrBuffer = null;

    public StreamReader(InputStream in, StringBuffer errStrBuffer) {
        this.in = in;
        this.errStrBuffer = errStrBuffer;
    }

    public void run() {
        try {
            logger.debug("Start to Read");

            errStrBuffer.append(IOUtils.toString(new InputStreamReader(in, "utf-8")));

            logger.debug("End to Read");
        }
        catch (java.nio.channels.ClosedByInterruptException ie) {
            logger.debug("StreamReader exception, ignore."); // ignore
        }
        catch (Exception e) {
            logger.error(e);
        }
    }

}
