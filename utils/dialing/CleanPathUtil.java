package com.ztesoft.zsmart.zcm.dialing.util;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by hu.bo on 2019/10/11.
 */
public abstract class CleanPathUtil {
    private static final Map<String, String> pathCharWhiteList = new HashMap<>(128);
    static {
        String pathCharWhiteListResources = "abcdefghijklmnopqrstuvwxyz1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ:_-./\\";
        for (int i = 0; i < pathCharWhiteListResources.length(); i++) {
            String c = String.valueOf(pathCharWhiteListResources.charAt(i));
            pathCharWhiteList.put(c, c);
        }
    }

    public static String cleanString(String filePath) {
        StringBuffer cleanString = new StringBuffer();
        String preStr = null;
        for (int i = 0; i < filePath.length(); i++) {
            String curStr = String.valueOf(filePath.charAt(i));
            String curListStr = pathCharWhiteList.get(curStr);
            if (curListStr == null) {
                continue;
            }
            else {
                if (preStr == null || !".".equals(preStr)) {
                    cleanString.append(curStr);
                    preStr = curStr;
                }
                else if (".".equals(preStr) && (!".".equals(curStr)) && (!"\\".equals(curStr)) && (!"/".equals(curStr))) {
                    cleanString.append(curStr);
                    preStr = curStr;
                }
            }
        }
        if (cleanString.charAt(0) == '.') {
            return cleanString.substring(1, cleanString.length());
        }
        else {
            return cleanString.toString();
        }
    }

}
