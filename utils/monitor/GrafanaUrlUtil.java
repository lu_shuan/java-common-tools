package com.ztesoft.zsmart.zcm.monitor.utils;

import com.ztesoft.zsmart.zcm.monitor.config.MonitorProperties;

public final class GrafanaUrlUtil {
    private GrafanaUrlUtil() {
    }

    private static MonitorProperties monitorProperties = SpringContextUtil.getBean(MonitorProperties.class);

    public static String getK2hRawHostMetricsUrl(String ip) {
        return monitorProperties.getHttpGrafanaUrl() + "/d/000000006/hostmetrics?orgId=1&refresh=10s&var-host=" + ip;
    }
}
