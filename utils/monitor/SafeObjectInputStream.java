package com.ztesoft.zsmart.zcm.monitor.utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectStreamClass;
import java.util.List;

public class SafeObjectInputStream extends ObjectInputStream {

    List<Class> classes;

    Boolean flag = false;

    public SafeObjectInputStream(InputStream in, List<Class> classes) throws IOException {
        super(in);
        this.classes = classes;
    }

    @Override
    protected Class<?> resolveClass(ObjectStreamClass desc) throws IOException, ClassNotFoundException {
        boolean notExist = true;
        for (Class c : classes) {
            if (desc.getName().equals(c.getName())) {
                notExist = false;
            }
        }
        if (!flag && notExist) {
            throw new ClassNotFoundException("SerializeUtil Unsupported Class: " + desc.getName());
        }
        this.flag = true;
        return super.resolveClass(desc);
    }
}