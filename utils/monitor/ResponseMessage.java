package com.ztesoft.zsmart.zcm.monitor.utils;

public class ResponseMessage<T> {

    private static final Object EMPTY_OBJ = "{}";

    public static final ResponseMessage SUCCESS_MSG = new ResponseMessage(EMPTY_OBJ,
        OperatorStateCode.SUCCESS_OPERATION);

    private T data;

    private String code;

    private String msg;

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResponseMessage(T data, String code, String msg) {
        super();
        this.data = data;
        this.code = code;
        this.msg = msg;
    }

    public ResponseMessage(T data) {
        super();
        this.data = data;
        this.code = SUCCESS_MSG.getCode();
        this.msg = SUCCESS_MSG.getMsg();
    }

    public ResponseMessage(T data, OperatorStateCode opst) {
        this.data = data;
        this.code = opst.code();
        this.msg = opst.msg();
    }

    public ResponseMessage() {
        // TODO Auto-generated constructor stub
    }

    public static ResponseMessage<Object> failedMsg(String code, String msg) {
        return new ResponseMessage<>(EMPTY_OBJ, code, msg);
    }

    public static ResponseMessage<Object> failedMsg(OperatorStateCode opst) {
        return new ResponseMessage<Object>(EMPTY_OBJ, opst.code(), opst.msg());
    }
}