package com.ztesoft.zsmart.zcm.monitor.utils;

/**
 * 统一提供内部FTP的连接信息
 */
public final class MoniParamLib {

    private static MoniParamLib sftpLib = null;

    private String serverIp;

    private int sftpPort;

    private String userName;

    private String password;

    private String daemonSyncFilesList;

    private String sftpPath;

    private String ctrlTimeout;

    private String kpiSendUrl;

    private Boolean agentAutoSync;

    private Integer agentAlarmTaskThreadPoolSize;

    private Integer agentAlarmTaskThreadMaxPoolSize;

    public static MoniParamLib getInstance() {
        synchronized (MoniParamLib.class) {
            if (sftpLib == null) {
                sftpLib = new MoniParamLib();
            }
        }
        return sftpLib;
    }

    private MoniParamLib() {
    }

    public String getIp() {
        return this.serverIp;
    }

    public void setIp(String serverIp) {
        this.serverIp = serverIp;
    }

    public int getPort() {
        return this.sftpPort;
    }

    public void setPort(int sftpPort) {
        this.sftpPort = sftpPort;
    }

    public String getUser() {
        return this.userName;
    }

    public void setUser(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDaemonSyncFilesList() {
        return this.daemonSyncFilesList;
    }

    public void setDaemonSyncFilesList(String daemonSyncFilesList) {
        this.daemonSyncFilesList = daemonSyncFilesList;
    }

    private String activemqIp;

    public String getActivemqIp() {
        return this.activemqIp;
    }

    public void setActivemqIp(String activemqIp) {
        this.activemqIp = activemqIp;
    }

    private String activemqPort;

    public String getActivemqPort() {
        return this.activemqPort;
    }

    public void setActivemqPort(String activemqPort) {
        this.activemqPort = activemqPort;
    }

    public String getSftpPath() {
        return this.sftpPath;
    }

    public void setSftpPath(String sftpPath) {
        this.sftpPath = sftpPath;
    }

    public String getCtrlTimeout() {
        return this.ctrlTimeout;
    }

    public void setCtrlTimeout(String ctrlTimeout) {
        this.ctrlTimeout = ctrlTimeout;
    }

    public String getKpiSendUrl() {
        return kpiSendUrl;
    }

    public void setKpiSendUrl(String kpiSendUrl) {
        this.kpiSendUrl = kpiSendUrl;
    }

    public Boolean getAgentAutoSync() {
        return agentAutoSync;
    }

    public void setAgentAutoSync(Boolean agentAutoSync) {
        this.agentAutoSync = agentAutoSync;
    }

    public Integer getAgentAlarmTaskThreadPoolSize() {
        return agentAlarmTaskThreadPoolSize;
    }

    public void setAgentAlarmTaskThreadPoolSize(Integer agentAlarmTaskThreadPoolSize) {
        this.agentAlarmTaskThreadPoolSize = agentAlarmTaskThreadPoolSize;
    }

    public Integer getAgentAlarmTaskThreadMaxPoolSize() {
        return agentAlarmTaskThreadMaxPoolSize;
    }

    public void setAgentAlarmTaskThreadMaxPoolSize(Integer agentAlarmTaskThreadMaxPoolSize) {
        this.agentAlarmTaskThreadMaxPoolSize = agentAlarmTaskThreadMaxPoolSize;
    }
}