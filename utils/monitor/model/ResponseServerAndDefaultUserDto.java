package com.ztesoft.zsmart.zcm.monitor.utils.model;


import java.util.Objects;

/**
 * 主机和主机的默认用户信息
 * 
 * @author t.hj
 */
public class ResponseServerAndDefaultUserDto extends Server {
    private Integer tenantId;

    private String userName;

    private String passWord;

    private ServerUser defaultUser;

    public Integer getTenantId() {
        return tenantId;
    }

    public void setTenantId(Integer tenantId) {
        this.tenantId = tenantId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }

    public ServerUser getDefaultUser() {
        return defaultUser;
    }

    public void setDefaultUser(ServerUser defaultUser) {
        this.defaultUser = defaultUser;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ResponseServerAndDefaultUserDto responseserveranddefaultuserdto = (ResponseServerAndDefaultUserDto) o;
        return Objects.equals(getServerId(), responseserveranddefaultuserdto.getServerId());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getServerId());
    }
}
