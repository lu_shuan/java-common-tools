package com.ztesoft.zsmart.zcm.monitor.utils.model;

/**
 * <Class Description>
 *
 * @author Administrator
 * @package com.ztesoft.zcm.cmdb.domain.extra
 * @date 2020/4/20 15:32
 */
public enum DeployMode {
    /**
     * 默认容器式部署
     */
    DOCKER,

    /**
     * DaemonSet模式部署，增加节点会自动部署
     */
    DAEMONSET,

    /**
     * Deployment模式部署
     */
    DEPLOYMENT;
}
