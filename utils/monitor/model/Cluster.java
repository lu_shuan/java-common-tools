package com.ztesoft.zsmart.zcm.monitor.utils.model;

import com.ztesoft.zsmart.core.jdbc.api.BaseBean;
import com.ztesoft.zsmart.core.jdbc.api.annotation.GeneratedValue;
import com.ztesoft.zsmart.core.jdbc.api.annotation.GenerationType;
import com.ztesoft.zsmart.core.jdbc.api.annotation.Id;
import com.ztesoft.zsmart.core.jdbc.api.annotation.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import java.util.Date;
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table("cluster")
public class Cluster extends BaseBean {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, sequence = "CLUSTER")
    private Integer clusterId;

    private String clusterCode;

    private String clusterType;

    private String masterApi;

    private String clusterDomain;

    private Integer tenantId;

    private String clusterName;

    private Integer createUserId;

    private Date createDate;

    private Date stateDate;

    private String state;

    private String applicationId;

    private DeployMode deployMode;

    private String extraParams;

    public Integer getClusterId() {
        return clusterId;
    }

    public void setClusterId(Integer clusterId) {
        this.clusterId = clusterId;
    }

    public String getClusterCode() {
        return clusterCode;
    }

    public void setClusterCode(String clusterCode) {
        this.clusterCode = clusterCode == null ? null : clusterCode.trim();
    }

    public String getClusterType() {
        return clusterType;
    }

    public void setClusterType(String clusterType) {
        this.clusterType = clusterType == null ? null : clusterType.trim();
    }



    public boolean isDockerType() {
        return !DeployMode.DAEMONSET.equals(this.deployMode) && !DeployMode.DEPLOYMENT.equals(this.deployMode);
    }

    public String getMasterApi() {
        return masterApi;
    }

    public void setMasterApi(String masterApi) {
        this.masterApi = masterApi == null ? null : masterApi.trim();
    }

    public String getClusterDomain() {
        return clusterDomain;
    }

    public void setClusterDomain(String clusterDomain) {
        this.clusterDomain = clusterDomain == null ? null : clusterDomain.trim();
    }

    public Integer getTenantId() {
        return tenantId;
    }

    public void setTenantId(Integer tenantId) {
        this.tenantId = tenantId;
    }

    public String getClusterName() {
        return clusterName;
    }

    public void setClusterName(String clusterName) {
        this.clusterName = clusterName == null ? null : clusterName.trim();
    }

    public Integer getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(Integer createUserId) {
        this.createUserId = createUserId;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getStateDate() {
        return stateDate;
    }

    public void setStateDate(Date stateDate) {
        this.stateDate = stateDate;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state == null ? null : state.trim();
    }
    public String getApplicationId() {
        return applicationId;
    }

    public DeployMode getDeployMode() {
        return deployMode;
    }

    public void setDeployMode(DeployMode deployMode) {
        this.deployMode = deployMode;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    public String getExtraParams() {
        return extraParams;
    }

    public void setExtraParams(String extraParams) {
        this.extraParams = extraParams;
    }

}