package com.ztesoft.zsmart.zcm.monitor.utils.model;

import com.ztesoft.zsmart.core.jdbc.api.BaseBean;
import com.ztesoft.zsmart.core.jdbc.api.annotation.GeneratedValue;
import com.ztesoft.zsmart.core.jdbc.api.annotation.GenerationType;
import com.ztesoft.zsmart.core.jdbc.api.annotation.Id;
import com.ztesoft.zsmart.core.jdbc.api.annotation.Table;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import java.util.Objects;

@Table("server")
@ApiModel(value = "Server", description = "主机实体类")
public class Server extends BaseBean {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, sequence = "SERVER")
    @ApiModelProperty(value = "主机ID", required = false, name = "serverId", dataType = "Integer")
    private Integer serverId;

    @ApiModelProperty(value = "设备编号", required = false, name = "serverName", dataType = "String")
    private String serverName;

    @ApiModelProperty(value = "IP地址", required = false, name = "ipAddr", dataType = "String")
    private String ipAddr;

    @ApiModelProperty(value = "管理IP地址", required = false, name = "mgrIpAddr", dataType = "String")
    private String mgrIpAddr;

    @ApiModelProperty(value = "私有IP地址", required = false, name = "privateIpAddr", dataType = "String")
    private String privateIpAddr;

    @ApiModelProperty(value = "设备ID", required = false, name = "deviceId", dataType = "Integer")
    private Integer deviceId;

    @ApiModelProperty(value = "资源类型ID", required = false, name = "resClsId", dataType = "Long")
    private Long resClsId;

    @ApiModelProperty(value = "操作系统", required = false, name = "operatingSystem", dataType = "String")
    private String operatingSystem;

    @ApiModelProperty(value = "CPU核数", required = false, name = "cpuCore", dataType = "Integer")
    private Integer cpuCore;

    @ApiModelProperty(value = "CPU主频", required = false, name = "cpuMainFrequency", dataType = "String")
    private String cpuMainFrequency;

    @ApiModelProperty(value = "内存", required = false, name = "memMb", dataType = "Integer")
    private Integer memMb;

    @ApiModelProperty(value = "磁盘", required = false, name = "diskGb", dataType = "Integer")
    private Integer diskGb;

    @ApiModelProperty(value = "ssh端口", required = false, name = "sshPort", dataType = "Integer")
    private Integer sshPort;

    @ApiModelProperty(value = "网络适配器", required = false, name = "netAdapter", dataType = "String")
    private String netAdapter;

    @ApiModelProperty(value = "入网时间", required = false, name = "netAccessDate", dataType = "Date")
    private Date netAccessDate;

    @ApiModelProperty(value = "维修制造商", required = false, name = "maintenanceManufacturer", dataType = "String")
    private String maintenanceManufacturer;

    @ApiModelProperty(value = "创建人", required = false, name = "createUserId", dataType = "Integer")
    private Integer createUserId;

    @ApiModelProperty(value = "创建时间", required = false, name = "createDate", dataType = "Date")
    private Date createDate;

    @ApiModelProperty(value = "状态变更时间", required = false, name = "stateDate", dataType = "Date")
    private Date stateDate;

    @ApiModelProperty(value = "状态", required = false, name = "state", dataType = "String")
    private String state;

    @ApiModelProperty(value = "GPU数量", required = false, name = "gpuNum", dataType = "Integer")
    private Integer gpuNum;

    @ApiModelProperty(value = "GPU型号", required = false, name = "gpuModel", dataType = "String")
    private String gpuModel;

    @ApiModelProperty(value = "GPU大小", required = false, name = "gpuMemMb", dataType = "Integer")
    private Integer gpuMemMb;

    @ApiModelProperty(value = "备注", required = false, name = "remarks", dataType = "String")
    private String remarks;

    @ApiModelProperty(value = "用途", required = false, name = "purpose", dataType = "String")
    private String purpose;

    @ApiModelProperty(value = "责任人", required = false, name = "responsible", dataType = "String")
    private String responsible;

    @ApiModelProperty(value = "虚机ID", required = false, name = "vimId", dataType = "String")
    private String vimId;

    @ApiModelProperty(value = "虚机主机ID", required = false, name = "vimServerId", dataType = "Integer")
    private String vimServerId;

    @ApiModelProperty(value = "docker连接端口", required = false, name = "dockerPort", dataType = "Integer")
    private Integer dockerPort;

    @ApiModelProperty(value = "docker https连接端口", required = false, name = "dockerSslPort", dataType = "Integer")
    private Integer dockerSslPort;

    @ApiModelProperty(value = "公网IP", required = false, name = "publicIpAddr", dataType = "String")
    private String publicIpAddr;



    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Server)) {
            return false;
        }
        Server server = (Server) o;
        return Objects.equals(getServerId(), server.getServerId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getServerId());
    }

    public Integer getServerId() {
        return serverId;
    }

    public void setServerId(Integer serverId) {
        this.serverId = serverId;
    }

    public String getServerName() {
        return serverName;
    }

    public void setServerName(String serverName) {
        this.serverName = serverName == null ? null : serverName.trim();
    }

    public String getIpAddr() {
        return ipAddr;
    }

    public void setIpAddr(String ipAddr) {
        this.ipAddr = ipAddr == null ? null : ipAddr.trim();
    }

    public String getMgrIpAddr() {
        return mgrIpAddr;
    }

    public void setMgrIpAddr(String mgrIpAddr) {
        this.mgrIpAddr = mgrIpAddr == null ? null : mgrIpAddr.trim();
    }

    public String getPrivateIpAddr() {
        return privateIpAddr;
    }

    public void setPrivateIpAddr(String privateIpAddr) {
        this.privateIpAddr = privateIpAddr == null ? null : privateIpAddr.trim();
    }

    public Integer getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Integer deviceId) {
        this.deviceId = deviceId;
    }

    public Long getResClsId() {
        return resClsId;
    }

    public void setResClsId(Long resClsId) {
        this.resClsId = resClsId;
    }

    public String getOperatingSystem() {
        return operatingSystem;
    }

    public void setOperatingSystem(String operatingSystem) {
        this.operatingSystem = operatingSystem == null ? null : operatingSystem.trim();
    }

    public Integer getCpuCore() {
        return cpuCore;
    }

    public void setCpuCore(Integer cpuCore) {
        this.cpuCore = cpuCore;
    }

    public String getCpuMainFrequency() {
        return cpuMainFrequency;
    }

    public void setCpuMainFrequency(String cpuMainFrequency) {
        this.cpuMainFrequency = cpuMainFrequency == null ? null : cpuMainFrequency.trim();
    }

    public Integer getMemMb() {
        return memMb;
    }

    public void setMemMb(Integer memMb) {
        this.memMb = memMb;
    }

    public Integer getDiskGb() {
        return diskGb;
    }

    public void setDiskGb(Integer diskGb) {
        this.diskGb = diskGb;
    }

    public Integer getSshPort() {
        return sshPort;
    }

    public void setSshPort(Integer sshPort) {
        this.sshPort = sshPort;
    }

    public String getNetAdapter() {
        return netAdapter;
    }

    public void setNetAdapter(String netAdapter) {
        this.netAdapter = netAdapter == null ? null : netAdapter.trim();
    }

    public Date getNetAccessDate() {
        return netAccessDate;
    }

    public void setNetAccessDate(Date netAccessDate) {
        this.netAccessDate = netAccessDate;
    }

    public String getMaintenanceManufacturer() {
        return maintenanceManufacturer;
    }

    public void setMaintenanceManufacturer(String maintenanceManufacturer) {
        this.maintenanceManufacturer = maintenanceManufacturer == null ? null : maintenanceManufacturer.trim();
    }

    public Integer getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(Integer createUserId) {
        this.createUserId = createUserId;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getStateDate() {
        return stateDate;
    }

    public void setStateDate(Date stateDate) {
        this.stateDate = stateDate;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state == null ? null : state.trim();
    }

    public Integer getGpuNum() {
        return gpuNum;
    }

    public void setGpuNum(Integer gpuNum) {
        this.gpuNum = gpuNum;
    }

    public String getGpuModel() {
        return gpuModel;
    }

    public void setGpuModel(String gpuModel) {
        this.gpuModel = gpuModel;
    }

    public Integer getGpuMemMb() {
        return gpuMemMb;
    }

    public void setGpuMemMb(Integer gpuMemMb) {
        this.gpuMemMb = gpuMemMb;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public String getResponsible() {
        return responsible;
    }

    public void setResponsible(String responsible) {
        this.responsible = responsible;
    }

    public String getVimId() {
        return vimId;
    }

    public void setVimId(String vimId) {
        this.vimId = vimId;
    }

    public String getVimServerId() {
        return vimServerId;
    }

    public void setVimServerId(String vimServerId) {
        this.vimServerId = vimServerId;
    }

    public Integer getDockerPort() {
        return dockerPort;
    }

    public void setDockerPort(Integer dockerPort) {
        this.dockerPort = dockerPort;
    }

    public Integer getDockerSslPort() {
        return dockerSslPort;
    }

    public void setDockerSslPort(Integer dockerSslPort) {
        this.dockerSslPort = dockerSslPort;
    }

    public String getPublicIpAddr() {
        return publicIpAddr;
    }

    public void setPublicIpAddr(String publicIpAddr) {
        this.publicIpAddr = publicIpAddr;
    }
}