package com.ztesoft.zsmart.zcm.monitor.utils.model;

import com.ztesoft.zsmart.core.jdbc.api.BaseBean;
import com.ztesoft.zsmart.core.jdbc.api.annotation.GeneratedValue;
import com.ztesoft.zsmart.core.jdbc.api.annotation.GenerationType;
import com.ztesoft.zsmart.core.jdbc.api.annotation.Id;
import com.ztesoft.zsmart.core.jdbc.api.annotation.Table;

import java.util.Date;

@Table("server_user")
public class ServerUser extends BaseBean {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, sequence = "SERVER_USER")
    private Integer serverUserId;

    private Integer serverId;

    private String userName;

    private String passWord;

    private String isDefault;

    private String hasTruskLink;

    private Integer createUserId;

    private Date createDate;

    private Date stateDate;

    private String state;

    public Integer getServerUserId() {
        return serverUserId;
    }

    public void setServerUserId(Integer serverUserId) {
        this.serverUserId = serverUserId;
    }

    public Integer getServerId() {
        return serverId;
    }

    public void setServerId(Integer serverId) {
        this.serverId = serverId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord == null ? null : passWord.trim();
    }

    public String getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(String isDefault) {
        this.isDefault = isDefault == null ? null : isDefault.trim();
    }

    public String getHasTruskLink() {
        return hasTruskLink;
    }

    public void setHasTruskLink(String hasTruskLink) {
        this.hasTruskLink = hasTruskLink == null ? null : hasTruskLink.trim();
    }

    public Integer getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(Integer createUserId) {
        this.createUserId = createUserId;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getStateDate() {
        return stateDate;
    }

    public void setStateDate(Date stateDate) {
        this.stateDate = stateDate;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state == null ? null : state.trim();
    }
}