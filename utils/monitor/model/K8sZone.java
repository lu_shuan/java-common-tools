package com.ztesoft.zsmart.zcm.monitor.utils.model;

import com.ztesoft.zsmart.core.jdbc.api.BaseBean;
import com.ztesoft.zsmart.core.jdbc.api.annotation.GeneratedValue;
import com.ztesoft.zsmart.core.jdbc.api.annotation.GenerationType;
import com.ztesoft.zsmart.core.jdbc.api.annotation.Id;
import com.ztesoft.zsmart.core.jdbc.api.annotation.Table;

import java.math.BigDecimal;
import java.util.Date;

@Table("k8s_zone")
public class K8sZone extends BaseBean {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, sequence = "K8S_ZONE")
    private Integer zoneId;

    private String type;

    private Integer k8sClusterId;

    private String zoneName;

    private String zoneKey;

    private String zoneValue;

    private String zoneType;

    private Integer createUserId;

    private Date createDate;

    private Date stateDate;

    private String state;

    private String vip;

    private BigDecimal cpuRequestRatio;

    private BigDecimal memRequestRatio;

    public Integer getZoneId() {
        return zoneId;
    }

    public void setZoneId(Integer zoneId) {
        this.zoneId = zoneId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type == null ? null : type.trim();
    }

    public Integer getK8sClusterId() {
        return k8sClusterId;
    }

    public void setK8sClusterId(Integer k8sClusterId) {
        this.k8sClusterId = k8sClusterId;
    }

    public String getZoneName() {
        return zoneName;
    }

    public void setZoneName(String zoneName) {
        this.zoneName = zoneName == null ? null : zoneName.trim();
    }

    public String getZoneKey() {
        return zoneKey;
    }

    public void setZoneKey(String zoneKey) {
        this.zoneKey = zoneKey == null ? null : zoneKey.trim();
    }

    public String getZoneValue() {
        return zoneValue;
    }

    public void setZoneValue(String zoneValue) {
        this.zoneValue = zoneValue == null ? null : zoneValue.trim();
    }

    public Integer getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(Integer createUserId) {
        this.createUserId = createUserId;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getStateDate() {
        return stateDate;
    }

    public void setStateDate(Date stateDate) {
        this.stateDate = stateDate;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state == null ? null : state.trim();
    }

    public String getZoneType() {
        return zoneType;
    }

    public void setZoneType(String zoneType) {
        this.zoneType = zoneType;
    }

    public String getVip() {
        return vip;
    }

    public void setVip(String vip) {
        this.vip = vip;
    }

    public BigDecimal getCpuRequestRatio() {
        return cpuRequestRatio;
    }

    public void setCpuRequestRatio(BigDecimal cpuRequestRatio) {
        this.cpuRequestRatio = cpuRequestRatio;
    }

    public BigDecimal getMemRequestRatio() {
        return memRequestRatio;
    }

    public void setMemRequestRatio(BigDecimal memRequestRatio) {
        this.memRequestRatio = memRequestRatio;
    }
}