package com.ztesoft.zsmart.zcm.monitor.utils;

import javax.sql.DataSource;

import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.jdbc.core.JdbcTemplate;

/** 
 * <Description> <br> 
 *  jdbc数据库连接工具
 * @author wang.chang<br>
 * @version 1.0<br>
 * @taskId <br>
 * @CreateDate 2019年12月2日 <br>
 * @since V9<br>
 * @see com.ztesoft.zsmart.zcm.monitor.utils <br>
 */
public final class JdbcUtil {

    private JdbcUtil() {
    }

    /**
     * Creates the data source.
     *
     * @param type the type
     * @param url the url
     * @param user the user
     * @param password the password
     * @return the data source
     */
    public static DataSource createDataSource(String type, String url, String user, String password) {
        DataSourceBuilder builder = DataSourceBuilder.create();
        String driver = "com.mysql.jdbc.Driver";
        if ("oracle".equals(type)) {
            driver = "oracle.jdbc.driver.OracleDriver";
        }
        builder.driverClassName(driver);
        builder.url(url);
        builder.username(user);
        builder.password(password);
        DataSource dataSource = builder.build();
        return dataSource;
    }

    /**
     * Gets the jdbc template.
     *
     * @param dataSource the data source
     * @return the jdbc template
     */
    public static JdbcTemplate getJdbcTemplate(DataSource dataSource) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate();
        jdbcTemplate.setDataSource(dataSource);
        return jdbcTemplate;
    }
}