package com.ztesoft.zsmart.zcm.monitor.utils;

import java.time.ZoneId;

/**
 * <Description> 时区工具类
 * 
 * @version 1.0
 * @Author cwk
 * @Date 2019/3/7
 * @since V1.0
 * @see com.ztesoft.zsmart.zcm.monitor.utils
 */
public final class ZoneUtil {
    /**
     * 中国时区编号
     */
    private static final String TIME_ZONE_SH = "Asia/Shanghai";

    /**
     * 私有构造方法
     */
    private ZoneUtil() {

    }

    /**
     * 判断是否是中国时区
     * 
     * @return
     */
    public static Boolean isSHZone() {
        String zone = getZone();
        return !zone.isEmpty() && zone.equals(TIME_ZONE_SH);
    }

    /**
     * 获取当前系统时区
     * 
     * @return
     */
    public static String getZone() {
        return ZoneId.systemDefault().toString();
    }

}
