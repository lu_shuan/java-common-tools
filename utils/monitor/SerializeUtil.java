package com.ztesoft.zsmart.zcm.monitor.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectOutputStream;
import java.util.List;

import org.apache.ibatis.cache.CacheException;

public final class SerializeUtil {

    private SerializeUtil() {
        // prevent instantiation
    }

    public static byte[] serialize(Object object) {
        ObjectOutputStream oos = null;
        ByteArrayOutputStream baos = null;
        try {
            baos = new ByteArrayOutputStream();
            oos = new ObjectOutputStream(baos);
            oos.writeObject(object);
            return baos.toByteArray();
        }
        catch (Exception e) {
            throw new CacheException(e);
        }
    }

    public static Object unserialize(byte[] bytes, List<Class> classes) {
        if (bytes == null) {
            return null;
        }
        ByteArrayInputStream bais = null;
        try {
            bais = new ByteArrayInputStream(bytes);
            SafeObjectInputStream ois = new SafeObjectInputStream(bais, classes);
            return ois.readObject();
        }
        catch (Exception e) {
            throw new CacheException(e);
        }
    }
}
