package com.ztesoft.zsmart.zcm.monitor.utils;

public enum OperatorStateCode {

    SUCCESS_OPERATION("9999", "Operation success!"), FAILED_OPERATION("-1", "Operation Failed!"),
    DO_NOTHING_OPERATION("-2", "Nothing to do!"), BAD_INFDB_LINK_FAIL("ZCM-40001", ""),
    BAD_SERVICE_LINK_FAIL("ZCM-40002", ""), BAD_LOCALDB_LINK_FAIL("ZCM-40003", ""),
    ERROR_DATE_FORMAT_FAIL("ZCM-40004", ""), SYSTEM_PARAM_FAIL("ZCM-40005", ""),
    FILE_PATH_NOT_FOUND("ZCM-40006", ""), BAD_APPLICATION_LINK_FAIL("ZCM-40007", ""),
    MAIL_SEND_FAIL("ZCM-40008", ""), BAD_WARN_RULE_DATA_FAIL("ZCM-44001", "Get Rule Data Fail!"),
    PARSE_ALARM_FAILED("ZCM-40009", "Parse alarm failed."), CONFIRM_ALARM_FAILED("ZCM-40010", "Confirm alarm failed.");


    private final String code;

    private final String msg;

    OperatorStateCode(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public String code() {
        return code;
    }

    public String msg() {
        return msg;
    }
}
