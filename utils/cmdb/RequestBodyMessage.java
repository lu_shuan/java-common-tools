package com.ztesoft.zcm.cmdb.util;

public class RequestBodyMessage<T> {

    private T data;

    private PageRequestPlus page;

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public PageRequestPlus getPage() {
        return page;
    }

    public void setPage(PageRequestPlus page) {
        this.page = page;
    }
}
