package com.ztesoft.zcm.cmdb.util;

import com.ztesoft.zsmart.core.exception.BaseAppException;
import com.ztesoft.zsmart.core.log.ZSmartLogger;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

@Component("fileDownloadHelper")
public class FileDownloadHelper {
    private static final ZSmartLogger LOGGER = ZSmartLogger.getLogger(FileDownloadHelper.class);
    private static final int readSize = 4096;

    public void downloadExcelFile(HttpServletResponse response, String filePath, boolean isFromClassPath) throws BaseAppException {
        response.reset();
        response.setContentType("application/vnd.ms-excel;charset=utf-8");

        InputStream inputStream = null;
        OutputStream outputStream = null;
        String fileName = null;
        try {
            if (isFromClassPath) {
                ClassPathResource classPathResource = new ClassPathResource(filePath);
                fileName = classPathResource.getFile().getName();
                inputStream = classPathResource.getInputStream();
            }
            else {
                fileName = new File(filePath).getName();
                inputStream = new FileInputStream(filePath);
            }
            String fileType = fileName.substring(fileName.lastIndexOf("."), fileName.length());
            response.setHeader("Content-Disposition", "attachment;filename=" + DateUtil.getStandardCurrentDay() + "_template" + fileType);
            outputStream = response.getOutputStream();

            int len = 0;
            byte[] buffer = new byte[readSize];
            while ((len = inputStream.read(buffer)) > 0) {
                outputStream.write(buffer, 0, len);
                outputStream.flush();
            }
        }
        catch (IOException e) {
            LOGGER.error(e);
            throw new BaseAppException("ZCM-CMDB-10013", "File " + filePath + " does not exists");
        }
        catch (Exception e) {
            LOGGER.error(e);
            throw new BaseAppException("ZCM-CMDB-10014", "Download " + filePath + " error");
        }
        finally {
            try {
                if (null != inputStream) {
                    inputStream.close();
                }
                if (null != outputStream) {
                    outputStream.flush();
                    outputStream.close();
                }
            }
            catch (IOException e) {
                LOGGER.error(e);
            }
        }
            /*File path = new File(ResourceUtils.getURL("classpath:").getPath());
            File file = new File(path + "/static/" + fileName + ".pdf");
            fis = new FileInputStream(file);
            response.setHeader("Content-Disposition", "attachment; filename=" + file.getName());
            IOUtils.copy(fis, response.getOutputStream());
            response.flushBuffer();*/
    }
}
