package com.ztesoft.zcm.cmdb.util;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

/**
 * Created by xinshu on 2016/5/27.
 */
public class PageRequestPlus extends PageRequest {

    public PageRequestPlus(int page, int size, Sort.Direction direction, String... properties) {
        super(page, size, direction, properties);
    }

    // Modified by Mao.guangdong
    @JsonCreator
    public PageRequestPlus(@JsonProperty("page") int page, @JsonProperty("size") int size) {
        super(page, size);
    }

    public PageRequestPlus() {
        super(0, 10);
    }

}
