package com.ztesoft.zcm.cmdb.util;

import java.util.Collection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class ValidatorUtil {
    private ValidatorUtil() {

    }


    public static final boolean isValidIp(String ipAddr) {
        String regex = "^(\\d{1,2}|1\\d\\d|2[0-4]\\d|25[0-5])\\.(\\d{1,2}|1\\d\\d|2[0-4]\\d|25[0-5])\\.(\\d{1,2}|1\\d\\d|2[0-4]\\d|25[0-5])\\.(\\d{1,2}|1\\d\\d|2[0-4]\\d|25[0-5])$";
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(ipAddr);
        return m.find();
    }

    public static final boolean validateNotEmpty(String str) {
        if (validateNotNull(str)) {
            return !"".equals(str.trim());
        }
        return false;
    }

    public static final boolean validateNotNull(String str) {
        return str != null;
    }

    public static boolean validateNotNull(Object o) {
        return o != null;
    }

    public static boolean validateNotEmpty(Object[] obj) {
        if (validateNotNull(obj)) {
            return (obj.length != 0);
        }
        return false;
    }

    public static boolean validateNotEmpty(Collection col) {
        return !col.isEmpty();
    }
}
