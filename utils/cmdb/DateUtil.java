package com.ztesoft.zcm.cmdb.util;

import com.ztesoft.zsmart.core.log.ZSmartLogger;
import org.apache.commons.lang.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public final class DateUtil {
    public static final String DEFAULT_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";

    public static final String DEFAULT_DAY_FORMAT = "yyyy-MM-dd";

    private static final ZSmartLogger LOGGER = ZSmartLogger.getLogger(DateUtil.class);

    private DateUtil() {

    }

    public static SimpleDateFormat getDateFormat(String var0) {
        return new SimpleDateFormat(var0);
    }

    public static String getStandardCurrentDay() {
        SimpleDateFormat var0 = getDateFormat(DEFAULT_DAY_FORMAT);
        return var0.format(new Date());
    }

    public static String getStandardCurrentTime() {
        SimpleDateFormat var0 = getDateFormat(DEFAULT_TIME_FORMAT);
        return var0.format(new Date());
    }

    public static String formatDate(Date date, String format) {
        if (StringUtils.isEmpty(format)) {
            format = DEFAULT_TIME_FORMAT;
        }
        SimpleDateFormat var0 = getDateFormat(format);
        return var0.format(date);
    }

    public static Date string2Date(String str, String format) {
        if (StringUtils.isEmpty(format)) {
            format = DEFAULT_TIME_FORMAT;
        }
        SimpleDateFormat var0 = getDateFormat(format);
        try {
            return var0.parse(str);
        }
        catch (ParseException e) {
            LOGGER.error(e);
        }
        return null;
    }
}
