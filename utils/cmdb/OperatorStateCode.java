package com.ztesoft.zcm.cmdb.util;

public enum OperatorStateCode {
    FAILED_OPERATION("9998", "operation failed"),
    SUCESS_OPERATION("9999", "operation success"),

    SERVER_IS_NULL("ZCM-CMDB-10001", "Param server is null."),
    SERVER_IP_IS_NULL("ZCM-CMDB-10002", "Server IP is empty."),
    SERVER_IP_EXISTS("ZCM-CMDB-10003", "Server IP already exists."),
    SERVER_NOT_EXISTS("ZCM-CMDB-10004", "Server does not exists."),
    SERVER_IP_NOT_VALID("ZCM-CMDB-10005", "Server IP is not valid."),

    BUSINESS_DOMAIN_CODE_EXISTS("ZCM-CMDB-20001", "Business domain code already exists."),
    BUSINESS_DOMAIN_HAS_APP("ZCM-CMDB-20002", "There're associated applications under the business domain."),
    DEL_DEFAULTBUSINESS_DOMAIN("ZCM-CMDB-20003", "Default business domain does not allow deletion."),

    APPLICATION_NOT_EXIT("ZCM-CMDB-30001", "there's no application"),
    APPLICATIONINSTANCE_NOT_EXIT("ZCM-CMDB-30002", "there's no application instance"),

    TENANT_ID_NOT_EXIT("ZCM-CMDB-40001", "tenantId is empty"),

    PROJECT_CATALOG_CREATE_ERROR("6001", "create project catalog error"),
    PROJECT_CATALOG_DELETE_ERROR("6002", "delete project catalog error"),
    PROJECT_CATALOG_UPDATE_ERROR("6003", "update project catalog error"),
    PROJECT_CATALOG_SELECT_ERROR("6004", "select project catalog error"),
    PROJECT_DELETE_HAS_APPLICATION_ERROR("ZCM-65003", "project delete has application"),

    PROJECT_CREATE_ERROR("ZCM-65001", "create project error"),
    PROJECT_DELETE_ERROR("5002", "delete project error"),
    PROJECT_UPDATE_ERROR("ZCM-65002", "update project error"),
    PROJECT_CODE_EXISIST("ZCM-65004", "project code exsist"),
    PROJECT_CODE_NO_EXISIST("ZCM-65006", "project code no exsist"),
    PROJECT_SELECT_ERROR("5004", "select project error"),
    PROJECT_NOT_EXISTED("ZCM-65100", "Project Not Existed"),
    CREATE_REGISTRY_PROJECT_ERROR("ZCM-65005", "Create Registry Project Error"),



    K8S_ZONE_HAS_ALREADY_EXIST("ZCM-CMDB-40001", ""),
    K8S_ZONE_CREATE_FAIL("ZCM-CMDB-40002", ""),
    K8S_ZONE_MUST_HAS_SERVERS("ZCM-CMDB-40003", ""),
    K8S_ZONE_REMOVE_FAIL("ZCM-CMDB-40004", "");
    
    private final String code;

    private final String msg;

    OperatorStateCode(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public String code() {
        return code;
    }

    public String msg() {
        return msg;
    }

    @Override
    public String toString() {
        return code;
    }
}
