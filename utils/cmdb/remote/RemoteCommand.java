package com.ztesoft.zcm.cmdb.util.remote;

import lombok.Data;

/**
 * @author li.peilong
 * @date 2019/05/24
 **/
@Data
public class RemoteCommand {
    private String host;
    /**
     * 获取远程连接最长等待时间，单位：毫秒
     */
    private int maxWait = 3000;
    private String cmd;
    /**
     * 命令执行超时时间, 单位：秒
     */
    private int timeout = 5;

    public int getMaxWait() {
        if (maxWait <= 0) {
            return 3000;
        }
        return maxWait;
    }

    public int getTimeout() {
        if (timeout <= 0) {
            return 5;
        }
        return timeout;
    }
}
