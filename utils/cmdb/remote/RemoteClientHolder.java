package com.ztesoft.zcm.cmdb.util.remote;

import lombok.Data;

/**
 * @author li.peilong
 * @date 2019/05/23
 **/
@Data
public class RemoteClientHolder {
    private RemoteClientPool pool;
    private RemoteClient remoteClient;
    /**
     * 最后被使用时间，单位：ms
     * 后期做清理可以使用到
     */
    private volatile long lastActiveTime;
    public RemoteClientHolder(RemoteClientPool pool, RemoteClient remoteClient) {
        this.pool = pool;
        this.remoteClient = remoteClient;
        this.lastActiveTime = System.currentTimeMillis();
    }

    @Override
    public String toString() {
        return "RemoteClientHolder{" +
                "remote host=" + remoteClient.getHostAccessInfo().getHost() +
                '}';
    }
}
