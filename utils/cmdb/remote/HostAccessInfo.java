package com.ztesoft.zcm.cmdb.util.remote;

import com.jcraft.jsch.UserInfo;
import lombok.Builder;
import lombok.Data;

/**
 * @author li.peilong
 * @date 2019/05/24
 **/
@Data
@Builder
public class HostAccessInfo implements UserInfo {
    private String host;
    @Builder.Default
    private int port = 22;
    private String user;
    private String password;
    private String keyfile;
    private String passphrase;
    /**
     * 连接超时时间，单位：秒
     */
    @Builder.Default
    private int connTimeout = 3;

    @Override
    public boolean promptPassword(String s) {
        return true;
    }

    @Override
    public boolean promptPassphrase(String s) {
        return true;
    }

    @Override
    public boolean promptYesNo(String s) {
        return true;
    }

    @Override
    public void showMessage(String s) {

    }
}
