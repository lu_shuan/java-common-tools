package com.ztesoft.zcm.cmdb.util.remote;

import lombok.Data;

/**
 * 表示一个Shell命令执行结果
 *
 * @author li.peilong
 * @date 2019/01/07
 **/
@Data
public class RemoteResult {
    private int exitCode;
    private String output;
    private String error;
}
