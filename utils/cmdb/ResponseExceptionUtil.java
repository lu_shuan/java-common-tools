package com.ztesoft.zcm.cmdb.util;

import com.ztesoft.zsmart.core.exception.BaseAppException;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * 异常帮助类
 * @author t.hj
 */
@Aspect
@Component
public class ResponseExceptionUtil {
    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(ResponseExceptionUtil.class);
    /**
     * 公共资源模型controller返回值包装
     */
    @Pointcut("execution(public * com.ztesoft.zcm.cmdb.controller..*Controller.*(..))")
    public void resourceController(){}

    @Around(value = "resourceController()")
    public Object formatReturnning (ProceedingJoinPoint proceedingJoinPoint) {
        String funcName = proceedingJoinPoint.getSignature().getName();
        LOGGER.info(funcName+" start");
        try {
            //调用执行目标方法
            Object obj = proceedingJoinPoint.proceed();
            LOGGER.info(funcName+" success");
            return obj;
        } catch (BaseAppException exception) {
            exception.printStackTrace();
            ResponseMessage responseMessage = ResponseMessage.failedMsg(exception.getCode(), exception.getMessage());
            LOGGER.error(funcName+" error, "+exception.getMessage());
            return responseMessage;
        } catch (Throwable e) {
            e.printStackTrace();
            ResponseMessage responseMessage = ResponseMessage.failedMsg( OperatorStateCode.FAILED_OPERATION);
            responseMessage.setMsg(e.getMessage());
            LOGGER.error(funcName+" error, "+e.getMessage());
            return responseMessage;
        }
    }
}
