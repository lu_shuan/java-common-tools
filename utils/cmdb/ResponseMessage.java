package com.ztesoft.zcm.cmdb.util;

import org.springframework.data.domain.PageImpl;

import java.util.List;

public class ResponseMessage<T> {

    private static final Object EMPTY_OBJ = "{}";

    public static final ResponseMessage<Object> SUCCESS_MSG = new ResponseMessage<>(EMPTY_OBJ,
        OperatorStateCode.SUCESS_OPERATION);

    private T data;

    private String code;

    private String msg;

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResponseMessage() {
        super();
    }

    public ResponseMessage(T data, String code, String msg) {
        super();
        this.data = data;
        this.code = code;
        this.msg = msg;
    }

    public ResponseMessage(T data, OperatorStateCode opst) {
        this.data = data;
        this.code = opst.code();
        this.msg = opst.msg();
    }

    public static ResponseMessage<Object> failedMsg(String code, String msg) {
        return new ResponseMessage<>(EMPTY_OBJ, code, msg);
    }

    public static ResponseMessage<Object> failedMsg(OperatorStateCode opst) {
        return new ResponseMessage<Object>(EMPTY_OBJ, opst.code(), opst.msg());
    }

    public ResponseMessage(List<?> list, String code, String msg) {
        super();
        if (list != null) {
            this.data = (T) new PageImpl(list);
        }
        this.code = code;
        this.msg = msg;
    }

    public ResponseMessage(List<?> list, OperatorStateCode opst) {
        super();
        if (list != null) {
            this.data = (T) new PageImpl(list);
        }
        this.code = opst.code();
        this.msg = opst.msg();
    }

    public static ResponseMessage<Object> failedMsgWithStateCode(OperatorStateCode opst) {
        return new ResponseMessage<>(opst, opst.code(), opst.msg());
    }
}
