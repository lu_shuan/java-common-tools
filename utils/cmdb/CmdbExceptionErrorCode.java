package com.ztesoft.zcm.cmdb.util;

import com.ztesoft.zsmart.zcm.core.exception.ExceptionErrorCode;

/**
 * @author li.peilong
 * @date 2019/05/28
 **/
public enum CmdbExceptionErrorCode implements ExceptionErrorCode {
    REMOTE_CONNECT_TO_HOST_FAILED("ZCM-CMDB-00001"),
    REMOTE_EXEC_COMMAND_FAILED("ZCM-CMDB-00002"),
    REMOTE_EXEC_COMMAND_TIMEOUT("ZCM-CMDB-00003"),
    REMOTE_UNKNOWN_HOST("ZCM-CMDB-00004"),
    REMOTE_GET_ACCESS_INFO_FAILED("ZCM-CMDB-00005"),
    REMOTE_GET_CLIENT_FAILED("ZCM-CMDB-00006"),
    REMOTE_GET_CLIENT_TIMEOUT("ZCM-CMDB-00007");
    /**
     * code
     */
    private String code;

    /**
     * constructor
     *
     * @param code String
     */
    CmdbExceptionErrorCode(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return code;
    }
}
